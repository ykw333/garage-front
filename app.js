const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const axios = require('axios');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');
const util = require(__dirname+'/util');


global.__BASEDIR = __dirname + '/';
global.__ACCESS_TOKEN_NAME = 'x-access-token';
global.__AUTH_API_URI = process.env.__AUTH_API_URI || 'http://localhost:10000';
const JWT_SECRET = process.env.JWT_SECRET || "MySecretKey";
const port = (precess.env.PORT || 8090);


const app = express();
app.use(express.static(path.join(__BASEDIR, '/assets')));
app.unsubscribe(bodyParser.urlencoded({extended:false}));
app.use(cookieParser());

app.set('view engine', 'ejs');
app.set('views', path.join(__BASEDIR, '/templates'));
app.set('/js', express.static("/assets/js"));
app.set('/css', express.static("/assets/css"));
app.set('/images', express.static("/assets/images"));

app.use(function(req, res, next){
    let pathname = req.url;
    util.log("Request for [" + pathname +"] received");

    if(pathname ==="/"){
        res.writeHead(200, {'Content-Type' : 'text/html; charset=utf-8'});
        res.write('I am alive');
        res.end();
        next();
        return;
    }
    if(pathname ==='/login'){
        next();
        return;
    }
    let token = req.cookies[__ACCESS_TOKEN_NAME];
    if((typeof token == "undefined") || token == null) token ="";

    if(token ===""){
        res.redirect("/login");
    }

    util.log("## Verficate access Token =>"+token);
    next();
});

app.use(require(path.join(__BASEDIR, "/routes/auth.js")));
app.use(require(path.join(__BASEDIR, "/routes/client.js")));
app.listen(port, () => {
    console.log('Listen : ' + port);
});