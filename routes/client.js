const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');

const router = express.Router();
router.use(bodyParser.urlencoded({extended:false}));
router.use(cookieParser());

router.get("/client", (req, res) => {
    util.log("[Request : get /client] with no inval");
    const dataVal = { 
                        'bzno' : null,
                        'custnm' : null
                    }
                                    
    res.render("client/search",dataVal);
});

router.get("/client/:inval", (req, res) => {
    util.log("[Request : get /client]");
    const inval = req.params.inval;
    util.log("[Rqeust Data (inval)] : " + inval);
    const parsed = parseInt(inval, 10);
    const dataVal = isNaN(parsed) ? {
                                        'bzno' : '',
                                        'custnm' : parsed
                                    }
                                    : {
                                        'bzno' : parsed,
                                        'custnm' : ''
                                      }
    res.render("client/search",dataVal);
});

router.get("/home/home", (req, res)=>{
    util.log("[Request : get /home/home]");
    console.log("=================req.body.empno" + req.body.empno);

    let body = req.body;

    axios.post(__AUTH_API_URI + "/user/users",
        {
            empNo : req.body.empno
        }
    )
    .then((ret) => {
        if(ret.status == 200){
            util.log("[Return data]" + ret.data);

            res.cookie(__ACCESS_TOKEN_NAME, JSON.stringify(ret.data));
            res.redirect("/home/home");
        }else{
            res.redirect("/login");
        }
    })
    .catch((error) => {
        console.log(error);
        res.redirect("/login");
    });
});

module.exports = router;