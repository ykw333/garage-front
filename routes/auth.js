const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');

const router = express.Router();
router.use(bodyParser.urlencoded({extended:false}));
router.use(cookieParser());

router.get("/login", (req, res) => {
    util.log("[Request : get /login]");
    res.cookie(__ACCESS_TOKEN_NAME, "");
    res.render("login/login", res.cookie[__ACCESS_TOKEN_NAME]);
});

router.post("/login", (req, res)=>{
    util.log("[Request : post /login]");
    console.log("=================req.body.empno" + req.body.empno);

    let body = req.body;

    axios.post(__AUTH_API_URI + "/user/users",
        {
            empNo : req.body.empno
        }
    )
    .then((ret) => {
        if(ret.status == 200){
            util.log("[Return data]" + ret.data);

            res.cookie(__ACCESS_TOKEN_NAME, JSON.stringify(ret.data));
            res.redirect("/home/home");
        }else{
            res.redirect("/login");
        }
    })
    .catch((error) => {
        console.log(error);
        res.redirect("/login");
    });
});

router.get("/home/home", (req, res) => {
    util.log("[Request : get /home/home]");
    res.render("/home/home", JSON.parse(req.cookie[__ACCESS_TOKEN_NAME]));
})

module.exports = router;